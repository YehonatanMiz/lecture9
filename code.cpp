#include<iostream>

using std::cout;
using std::endl;

template<class T>
class TreeNode {
public:
	TreeNode(int value, TreeNode* right = NULL, TreeNode* left = NULL) :_value(value), _right(right), _left(left) { }
	TreeNode* getRight() const { return _right; }
	TreeNode* getLeft() const { return _left; }
	int getValue() const { return _value; }
private:
	int _value;
	TreeNode* _right;
	TreeNode* _left;

};

template<class T>
void printTree(TreeNode<T>* root) {
	if (root) {
		printTree(root->getRight());
		printTree(root->getLeft());
		cout << '(' << root->getValue() << ')' << endl << " |" << endl;
	}
}

int main()
{
	// leafs
	TreeNode<int>* a = new TreeNode<int>(6);
	TreeNode<int>* b = new TreeNode<int>(7);
	TreeNode<int>* c = new TreeNode<int>(8);
	// nodes
	TreeNode<int>* d = new TreeNode<int>(4, NULL, a);
	TreeNode<int>* e = new TreeNode<int>(5, c, b);
	// root
	TreeNode<int>* root = new TreeNode<int>(3, e, d);

	printTree<int>(root);

	return 0;
}
